from django import forms

class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(label='Event Name', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'write your event name here'}))

class FormPendaftar(forms.Form):
    nama_pendaftar = forms.CharField(label='Wanna join? Register your name here!', max_length=50,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'write your name here'}))
