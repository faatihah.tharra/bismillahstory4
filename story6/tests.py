from django.test import TestCase, Client
from django.urls import resolve

from .models import Kegiatan, Pendaftar
from . import views

class UnitTestForStory6(TestCase):
    def test_main_page(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_template_story6(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_eksistensi_judul(self):
        response = Client().get('/story6/')
        content = response.content.decode('utf8')
        self.assertIn("Event Name:", content)
        self.assertIn("New Activity", content)

    def test_story6_tambah_kegiatan_valid(self):
        Client().post('/story6/submit-kegiatan', data={'nama_kegiatan': 'Rebahan 24/7'})
        jumlah = Kegiatan.objects.filter(nama='Rebahan 24/7').count()
        self.assertEqual(jumlah, 1)

    def test_story6_tambah_kegiatan_invalid(self):
        Client().post('/story6/submit-kegiatan', data={})
        jumlah = Kegiatan.objects.filter(nama='mangan sek').count()
        self.assertEqual(jumlah, 0)

    def test_story6_tambah_pendaftar_valid(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        Client().post('/story6/submit-pendaftar/' + str(obj.id), data={'nama_pendaftar': 'pasti aku'})
        jumlah = Pendaftar.objects.filter(nama='pasti aku').count()
        self.assertEqual(jumlah, 1)
    
    def test_story6_tambah_pendaftar_invalid_no(self):
        Client().post('/story6/submit-pendaftar', data={})
        jumlah = Pendaftar.objects.filter(nama='Donald Trump').count()
        self.assertEqual(jumlah, 0)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='Rebahan 24/7')
        kegiatan = Kegiatan.objects.get(nama='Rebahan 24/7')
        self.assertEqual(str(kegiatan), 'Rebahan 24/7')

    def test_story6_model_pendaftar(self):
        Pendaftar.objects.create(nama='mang oleh')
        pendaftar = Pendaftar.objects.get(nama='mang oleh')
        self.assertEqual(str(pendaftar), 'mang oleh')

    def test_delete(self):
        hapus = Kegiatan.objects.create(nama='coba')
        hapus.delete()
        self.assertEqual(0,Kegiatan.objects.all().count())
