from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import FormKegiatan, FormPendaftar
from .models import Kegiatan, Pendaftar

def story6(request):
    data = {
        'kegiatan': Kegiatan.objects.all(),
        'pendaftar': Pendaftar.objects.all(),
    }
    data_forms = {
        'form_kegiatan': FormKegiatan(),
        'form_pendaftar': FormPendaftar(),
    }
    response = {
        'forms': data_forms,
        'data': data
    }
    return render(request, 'story6.html',response)

def submit_pendaftar(request, pk):
    if request.method == 'POST':
        form_pendaftar = FormPendaftar(request.POST or None)
        if form_pendaftar.is_valid():
            kegiatan = Kegiatan.objects.get(id=pk)
            data_form = form_pendaftar.cleaned_data
            data_input = Pendaftar()
            data_input.nama = data_form['nama_pendaftar']
            data_input.kegiatan = kegiatan
            data_input.save()
            return redirect('/story6')
        else:
            return redirect('/story6')

def submit_kegiatan(request):
    if request.method == 'POST':
        form_kegiatan = FormKegiatan(request.POST or None)
        if form_kegiatan.is_valid():
            data_form = form_kegiatan.cleaned_data
            data = Kegiatan(nama=data_form['nama_kegiatan'])
            data.save()
            return redirect('/story6')
        else:
            return redirect('/story6')

def deleteKegiatan(request, pk):
    data = Kegiatan.objects.filter(nama=pk)
    data.delete()
    return redirect('/story6')
