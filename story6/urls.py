from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.story6,name='story6'),
    path('submit-kegiatan', views.submit_kegiatan,name='submit_kegiatan'),
    path('submit-pendaftar/<int:pk>', views.submit_pendaftar,name='submit_pendaftar'),
    path('delete/<str:pk>', views.deleteKegiatan,name='deleteKegiatan'),
]
