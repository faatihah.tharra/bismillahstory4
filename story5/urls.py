from django.urls import path
from . import views

app_name = 'story5'
urlpatterns = [
    path('', views.formStory5,name='story5'),
    path('post/', views.postStory5,name='postStory5'),
    path('delete/<str:pk_matkul>/', views.delete,name='delete'),
    path('<str:pk_matkul>', views.detailJadwal,name='story5details'),
]
