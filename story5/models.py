from django.db import models

class MataKuliah(models.Model):

    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    jumlahSks = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=2000)
    semesterTahun = models.CharField(max_length=100)
    ruangKelas = models.CharField(max_length=200)

    def __str__(self):
        return self.nama
