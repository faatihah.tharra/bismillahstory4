from django import forms
from .models import MataKuliah


class formMatkul(forms.Form):

    class Meta:
        model = MataKuliah
        fields = ('__all__')

    CATEGORY = (
        ('2019/2020', '2019/2020'),
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023'),
    )
    SKS = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
    )

    attrs = {'class': 'form-control'}
    nama = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Mata Kuliah ', max_length=100, required=True)
    dosen = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Nama Dosen ', max_length=100, required=True)
    jumlahSks = forms.IntegerField(widget=forms.Select(choices=SKS,
        attrs=attrs), label='Jumlah SKS ', required=True)
    deskripsi = forms.CharField(widget=forms.Textarea(
        attrs=attrs), label='Deskripsi ',max_length=2000, required=True)
    semesterTahun = forms.CharField(widget=forms.Select(
        choices=CATEGORY, attrs=attrs), label='Tahun Ajar ', max_length=100, required=True)
    ruangKelas = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Ruang Kelas ', max_length=100, required=True)
