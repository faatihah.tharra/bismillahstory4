from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('educations/', views.educations, name='educations'),
    path('skill/', views.skill, name='skill'),
    path('entah/', views.entah, name='entah'),
]