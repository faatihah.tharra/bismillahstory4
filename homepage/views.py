from django.shortcuts import render

def profile(request):
    return render(request, 'profile.html')

def educations(request):
    return render(request, 'educations.html')

def skill(request):
    return render(request, 'skill.html')

def entah(request):
    return render(request, 'entah.html')